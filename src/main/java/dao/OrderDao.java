package dao;


import model.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Order save(Order order) {
        if (order != null) {
            if (order.getId() == null) {
                em.persist(order);
            } else {
                em.merge(order);
            }
        }
        em.flush();
        return order;

    }
    @Transactional
    public List<Order> getAll() {
        return em.createQuery("select o from Order o").getResultList();
    }
    @Transactional
    public Order getById(Long id) {
        TypedQuery<Order> query = em.createQuery("select o from Order o where o.id = :id", Order.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }
    @Transactional
    public void delete(Long id) {
        TypedQuery<Order> query = em.createQuery("delete from Order o where o.id = :id", Order.class);
        query.setParameter("id", id);
        query.executeUpdate();
    }
}
