package controller;

import dao.OrderDao;
import model.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {
    private OrderDao orderDao;
    private static final String ROUTESTRING = "orders";

    public OrderController(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @GetMapping(ROUTESTRING)
    public List<Order> getAll() {
        return orderDao.getAll();
    }
    @GetMapping(ROUTESTRING + "/{id}")
    public Order getById(@PathVariable Long id) {
        return orderDao.getById(id);
    }
    @PostMapping(ROUTESTRING)
    public Order createOrder(@RequestBody @Valid Order order) {
        return orderDao.save(order);
    }
    @DeleteMapping(ROUTESTRING + "/{id}")
    public void deleteOrder(@PathVariable Long id) {
        orderDao.delete(id);
    }
}
