package model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "orders")
public class Order {
    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "seq1", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Integer id;
    @Size(min=2)
    @Column(name = "order_number")
    private String orderNumber;
    @Valid
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
           name = "order_rows",
           joinColumns=@JoinColumn(name = "orders_id",
                   referencedColumnName = "id")
    )
    private List<OrderRow> orderRows;
}

